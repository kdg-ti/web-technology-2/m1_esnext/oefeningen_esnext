function summoner() {
  let monsters = [];
  
  let i = 1;
  while (i < 10) {
    let monster = function() { // create a monster function,
      return "Under attack of monster " + i; // that should show its number
    };
    monsters.push(monster); // and add it to the array
    i++;
  }
  
  // ...and return the array of monster summoning functions
  return monsters;
}

// TODO
//  print out the monsters with line numbers
//  correct the function to give each monster its own number


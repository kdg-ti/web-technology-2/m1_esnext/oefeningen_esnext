const actrices = [
	{
		naam: "Zoe Kravitz",
		geboorteJaar: 1988
	},
	{
    naam: "Emma Stone",
    geboorteJaar: 1988
	},
	{
		naam: "Emma Watson",
		geboorteJaar: 1990
	},
	{
		naam: "Natalie Portman",
		geboorteJaar: 1981
	},
	{
		naam: "Angelina Jolie",
		geboorteJaar: 1975
	},
	{
		naam: "Nathalie Meskens",
		geboorteJaar: 1982
	},
	{
		naam: "Reese Witherspoon",
		geboorteJaar: 1976
	},
	{
		naam: "Kirsten Dunst",
		geboorteJaar: 1982
	},
	{
		naam: "Kate Winslet",
		geboorteJaar: 1975
	},
	{
		naam: "Jennifer Lawrence",
		geboorteJaar: 1990
	},
	{
		naam: "Anne Hathaway",
		geboorteJaar: 1982
	},
	{
    naam: "Pommelien Thijs",
    geboorteJaar: 2001
	},
	{
		naam: "Drew Barrymore",
		geboorteJaar: 1975
	},
	{
		naam: "Julia Roberts",
		geboorteJaar: 1967
	},
	{
		naam: "Tara Reid",
		geboorteJaar: 1975
	},
	{
		naam: "Katie Holmes",
		geboorteJaar: 1978
	},
	{
		naam: "Marie Vinck",
		geboorteJaar: 1983
	}
];
